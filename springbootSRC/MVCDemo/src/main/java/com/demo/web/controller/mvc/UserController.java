package com.demo.web.controller.mvc;

import com.demo.web.entity.User;
import com.demo.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping("/user/{id}")
    @ResponseBody
    public User getUserById(@PathVariable Long id) {
        User user = userService.geUserById(id);
        return user;
    }
    @RequestMapping(method = RequestMethod.POST, path = "/post/user/{id}")
    @ResponseBody
    public User getUserByIdPost(@PathVariable Long id) {
        User user = userService.geUserById(id);
        return user;
    }
}
