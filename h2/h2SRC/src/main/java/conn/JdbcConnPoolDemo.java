package conn;

import org.h2.jdbcx.JdbcConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;

public class JdbcConnPoolDemo {
    public static void main(String[] args) throws SQLException {
        JdbcConnectionPool cp = JdbcConnectionPool.create("jdbc:h2:~/test","sa","sa");
        for (int i = 0; i < 100; i++) {
            System.out.println(i);
            Connection conn = cp.getConnection();
//            conn.createStatement().execute(args[i]);
            conn.close();
        }
        cp.dispose();

    }
}
