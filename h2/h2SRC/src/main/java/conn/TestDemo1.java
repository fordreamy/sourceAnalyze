package conn;

import org.h2.util.StringUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class TestDemo1 {
    int i = 1;
    protected int i2 = 2;

    public static void main(String[] args) throws Exception {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        String name = "123456";
        messageDigest.update(name.getBytes());
//        byte byteBuffer[] = messageDigest.digest();
        byte byteBuffer[] = new byte[5];
        byteBuffer[0] = 1;
        byteBuffer[1] = 20;
        byteBuffer[2] = 2;
        byteBuffer[3] = 127;
        byteBuffer[4] = -128;
        StringBuffer strHexString = new StringBuffer();
        for (int i = 0; i < byteBuffer.length; i++)
        {
            String hex = Integer.toHexString(0xff & byteBuffer[i]);
            if (hex.length() == 1)
            {
                strHexString.append('0');
            }
            strHexString.append(hex);
        }
        // 得到返回結果
        System.out.println(strHexString.toString());
        System.out.println(StringUtils.convertBytesToHex(byteBuffer,byteBuffer.length));

    }


}
