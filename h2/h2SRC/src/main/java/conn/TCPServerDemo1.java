package conn;

import org.h2.tools.Server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TCPServerDemo1 {
    public static void main(String[] args) throws Exception {
        Server server = Server.createTcpServer().start();
        Class.forName("org.h2.Driver");

        Connection conn = DriverManager.getConnection("jdbc:h2:tcp://localhost/~/test", "sa", "sa");
        System.out.println(conn);
        server.stop();
    }
}
