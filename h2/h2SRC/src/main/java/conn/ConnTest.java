package conn;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Enumeration;
import java.util.Vector;

public class ConnTest {
    static Logger logger = LogManager.getLogger();

    public static void main(String[] a) throws Exception {
        Class.forName("org.h2.Driver");
        logger.info("org.h2.Driver类加载完成:"+DriverManager.getDrivers().nextElement());

        Connection conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "123456");
        // add application code here
        logger.info(conn);
        /*while(true){
            if(System.currentTimeMillis()==20000){
                break;
            }
        }*/
        conn.close();
    }
}
