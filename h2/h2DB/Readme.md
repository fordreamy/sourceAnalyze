# h2DatabaseStudy

# 运行环境
  JDK7
  jetty-maven-plugin 版本9.2.20.v20161216
  tomcat7-maven-plugin

# 运行项目
  jetty:run
  tomcat7:run
  http://localhost:8080/h2DatabaseStudy
# 数据库
	以服务方式启动，在windows下双击h2-1.4.193.jar即可
	作为嵌入式则不用
	数据库启动后会生成数据库文件:.h2.server.properties、.db文件等
	windows下路径: C:\Users\用户名
	配置数据库路径: jdbc:h2:file:./db1 则会在项目根目录下生成db1.mv.db
# 连接数据库方式
	1.通过JDBC连接 (包dbConnection)
	2.通过Hibernate配置连接(通过SessionFactoryUtils获取session)
# 项目介绍
作为快速创建小型项目的基本框架，用于测试和写demo等
# 项目框架
	+ hibernate + h2
