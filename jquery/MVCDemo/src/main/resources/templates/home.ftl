<html>
<head>
    <title>Welcome!</title>
    <script src="/js/jquery-1.12.4.js"></script>
</head>
<body>
<h1>
${Session["user"].name}
</h1>
<script type="text/javascript">

    $(document).ready(function () {
        $("p").click(function () {
            $(this).hide();
        });
    });

</script>
<p>If you click on me, I will disappear.</p>
</body>
</html>